import React from 'react';
import './CountryInfo.css';


const CountryInfo = ({data}) => {
    return (
        <div className='CountryInfo'>
            <span className="CountryName"> Страна <h3>{data.name}</h3></span>
            <img src={data.flag} alt={data.name} className="Flag"/>
            <span className="СapitalName"> Столица <h4>{data.capital}</h4></span>

            <h5>Граничащие страны</h5>
            {data.bc.length ?
                <ul className='BordersName'>
                    {data.bc.map(border => {
                        return <li className='BorderName' key={border}>
                            {border}
                        </li>
                    })}
                </ul>
                : <div>Данные отсутсвуют...</div>}
            <span className="Maps"> По идее тут должна быть карта на основе кординат которые возвращает API,
                но блин почему то на моменте установки вываливается ошибка:
            <img src="/img/error.png" alt="причем тут ваще лоадер" className="Error"/>
            </span>

        </div>
    )
};

export default CountryInfo;
