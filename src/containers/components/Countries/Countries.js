import React  from 'react';
import './Countries.css';


const Countries = props => {

    return (
        <ul className='Countries'>
            {props.data.map(post => {
                return <li className='CountryName' key={post.code} onClick={() => props.getInfo(post.code)}>
                    {post.name}
                </li>
            })}
        </ul>
    );
};

export default Countries;
