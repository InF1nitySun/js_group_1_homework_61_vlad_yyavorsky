import React, {Component} from 'react';
import './App.css';
import axios from 'axios';
import Countries from "../components/Countries/Countries";
import CountryInfo from "../components/CountryInfo/CountryInfo";
import Loader from "../components/Loader/Loader";


class App extends Component {

    state = {
        info: null,
        catLoading: true,
        posts: [],
        borders: []

    };

    getNameCode() {
        const ALL_URL = '/all?fields=name;alpha3Code';
        axios.get(ALL_URL).then(response => {
            return Promise.all(response.data.map(post => {
                return {name: post.name, code: post.alpha3Code};
            }))
        }).then(posts => {
            const nposts = posts.sort((a, b) => {
                if (a.name < b.name) return -1;
                if (a.name > b.name) return 1;
                return 0;
            });
            this.setState({posts: nposts, catLoading: false});
        }).catch(error => {
            console.log(error);
        });
    };


    getInfo = async (code) => {
        const ALPHA_URL = '/alpha/';
        let response;
        try {
            response = await axios.get(ALPHA_URL + code);
        } catch (err) {
            console.log(err)
        }
        const borderCountries = await this.getBorder(response.data.borders);
        const bc = borderCountries.sort();
        const result = {...response.data, bc};
        this.setState({info: result});
    };

    getBorder = (borders) => {
        return new Promise((resolve, reject) => {
            Promise.all(borders.map(border => {
                return axios.get('/alpha/' + border)
            }))
                .then(response => {
                    const borderCountries = response.map((r) => r.data.name);
                    resolve(borderCountries)
                })
                .catch(err => reject(err))
        })
    };

    componentDidMount() {
        this.getNameCode();
    }

    render() {

        return (
            <div className="App">
                {this.state.catLoading ? <Loader/> : null}
                <h3>HomeWork 61</h3>
                <div className="Field">
                    <Countries data={this.state.posts} getInfo={this.getInfo}/>
                    {this.state.info ? <CountryInfo data={this.state.info}/> : <div className="SelectCountry">Выберите страну</div>}
                </div>
            </div>
        );
    }
}

export default App;
